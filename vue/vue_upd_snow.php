<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 15.05.2017
 * Time: 09:01
 */

$titre="Rent A Snow : Ajout de snow";

ob_start(); // Tampon de flux stocké en mémoire
$ligne=$resultat->fetch();
?>
<h2>Ajout de Snow</h2>
  <!-- code pour le formulaire d'ajout -->
<table class="table">
  <form class='form' method="post" action="index.php?action=vue_upd_snow">
    <tr>
      <td>ID Surf :</td>
      <!-- Mis le champ en readonly pour qu'il soit transmis par le POST ce qui n'est pas le cas du disabled -->
      <td><input type="text" placeholder="ID du snow" name="fID" value="<?=$ligne['idsurf'];?>" readonly></td>
    </tr>
    <tr>
      <td>Marque :</td>
      <td><input type="text" placeholder="Marque du snow" name="fMarque" value="<?=$ligne['marque'];?>"></td>
    </tr>
    <tr>
      <td>Boots :</td>
      <td><input type="text" placeholder="Type de Boots" name="fBoot" value="<?=$ligne['boots'];?>"></td>
    </tr>
    <tr>
      <td>Type :</td>
      <td><input type="text" placeholder="Type" name="fType" value="<?=$ligne['type'];?>"></td>
    </tr>
    <tr>
      <td>Disponibilité :</td>
      <td><input type="integer" placeholder="Disponibilité" name="fDispo" value="<?=$ligne['disponibilite'];?>"></td>
    </tr>
    <tr>
      <td><input type="submit" value="Envoyer"></td>
      <td><input type="reset" value="Effacer"></td>
    </tr>
  </form>
</table>


<?php
$contenu = ob_get_clean();
require 'gabarit.php';
